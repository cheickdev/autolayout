//
//  User.swift
//  Autolayout
//
//  Created by Cheick Mahady SISSOKO on 02/01/2016.
//  Copyright © 2016 Telecom ParisTech. All rights reserved.
//

import Foundation

struct User {
    let name: String
    let company: String
    let login: String
    let password: String
    
    static func login(login: String, password: String) -> User? {
        if let user = database[login] {
            if user.password == password {
                return user
            }
        }
        return nil
    }
    
    static let database: Dictionary<String, User> = {
        var theDatabase = Dictionary<String, User>()
        for user in [
            User(name: "Cheick Mahady SISSOKO", company: "Djamma", login: "cheick", password: "secret"),
            User(name: "Mina Mint Mohamed", company: "CM", login: "mina", password: "secret")
            ]{
            theDatabase[user.login] = user
        }
        return theDatabase
    }()
}